const fs = require('fs')
const path = require('path')

function writeToFile() {
  const fileName = process.argv.slice(2).shift()
  const filePath = path.join(process.cwd(), fileName)
  const content = process.argv.slice(3).join(' ').toString('utf-8')
  if (fs.existsSync(filePath)) {
    fs.appendFile(filePath, content, (err) => {
      if (err) throw err
    })
  } else {
    fs.writeFile(filePath, content, (err) => {
      if (err) throw err
    })
  }
}

writeToFile()
